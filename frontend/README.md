Repositorios
Spring boot app
https://github.com/noname07/ERPLab2

ReactJS app
https://github.com/noname07/ERPLab2React


Código fuente en google drive: https://drive.google.com/drive/folders/18lzeMHNe7oRoiIC0z1kG48UZkYCARzW6
Requisitos
Java 8 o superior.
Node. js.
Mysql y se recomienda mysql workbench.
Spring tool suite o eclipse + spring boot plug-in.
Node
Node es realmente fácil de instalar y ahora incluye NPM. Debería poder ejecutar el siguiente comando después del siguiente procedimiento de instalación.


$ node --version
v0.10.24

$ npm --version
1.3.21

Instalación de Node en Linux
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs

Instalación de Node en Windows
Simplemente vaya al sitio web oficial de Node.js y tome el instalador. Además, asegúrese de tener git disponible en su PATH, npm podría necesitarlo.


________________


Instalación
Si usted no cuenta con el código fuente puede descargarlo desde:

$ git clone https://github.com/noname07/ERPLab2React

Para descargar e instalar las dependencias del proyecto:

$ npm install

Correr la aplicación de React

$ npm start

Correr la aplicación de Spring-boot
$ git clone https://github.com/noname07/ERPLab2


Asegurarse de que MySql se encuentra instalado y que las credenciales correspondientes a localhost:3306 son usuario: root contraseña: root

Importar el proyecto en eclipse o en Spring Tool Suite.

Entrar al paquete com.ltlg.erplab

Ejecutar el main correspondiente a la clase ErplabApplication.

Correr el script Datos.sql

Usuario administrador: Lucas 
Contraseña: comoandas
