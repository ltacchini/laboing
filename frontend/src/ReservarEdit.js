import React, { Component } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import { Button, Container,CustomInput, FormText, Form, FormGroup, Label, Row, Input, Alert} from 'reactstrap';
import AppNavbar from './AppNavbar';
import Select from 'react-select';
import moment from 'moment';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class ReservarEdit extends Component {

    emptyItem = {
        idReserva: 0,
        fecha: moment().format('YYYY-MM-DD'),
        desde: '08:00:00',
        hasta: '09:00:00',
        activo: true,
        user: null,
        espacio: null,
        equipo: [],
    };

    constructor(props) {
        super(props);
        const cachedUser = localStorage.getItem('user');
        const user = JSON.parse(cachedUser);
        let rol;
        try {
          rol = JSON.parse(cachedUser).roles[0].role;
        } catch (e) {
          this.state = { isOpen: false, isLoading: false, role: '' };
        }

        this.emptyItem.user = user;
        this.state = {
            user: user,
            item: this.emptyItem,
            equipos: [],
            espacios: [],
            equiposEnUso: [],
            espacioEnUso: null, 
            role: rol
        };
        
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEquipos = this.handleEquipos.bind(this);
        this.handleEspacios = this.handleEspacios.bind(this);
    }
    
    async componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            const group = await (await fetch(`/reservas/${this.props.match.params.id}`)).json();
            this.setState({ item: group });
        }

        this.setEquipos();
        this.setEspacios();
    }

    setEspacios() {
        const { item } = this.state;
        let body = JSON.stringify(item);
        fetch('/reservas/espacios', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: body,
        })
            .then(response => response.json())
            .then(data => this.setState({ espacios: data }));
    }

    setEquipos() {
        const { item } = this.state;
        fetch('/reservas/equipos', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        })
            .then(response => response.json())
            .then(data => this.setState({ equipos: data }));
    }

    recalcularEquiposYEspacios() {
        let { item, equipos, espacios } = this.state;
        let espacioReservado = null;
        let equiposReservados = [];
        this.setEquipos();
        this.setEspacios();
        
        if (!(item.espacio === null)) {
            for (let e = 0; e < espacios.length; e++) {
                if (item.espacio.id === espacios[e].id && espacios[e].activo === false) {
                    espacioReservado = espacios[e];
                }
            }
        }
        if (!(item.equipo.length === 0)) {
            for (let i = 0; i < item.equipo.length; i++) {
                for (let e = 0; e < equipos.length; e++) {
                    if (equipos[e].activo === false && item.equipo[i].id === equipos[e].id) {
                        equiposReservados.push(equipos[e]);
                    }
                }
            }
        }
        this.setState({ espacioEnUso: espacioReservado, equiposEnUso: equiposReservados });
    }
   
    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let { item } = this.state;

        switch (name) {
            case 'desde':
                
                let inicio = value.split(':');
                
                inicio[1] = '00'
                inicio = inicio.join(':');
                item[name] = inicio + ':00';
                break;
                
            case 'hasta':
                let fin = value.split(':');

                fin[1] = '00'
                fin = fin.join(':');
                item[name] = fin + ':00';
                break;

            case 'fecha':
                item[name] = value;
                break;
                    
            default:
                item[name] = value;
            }

        this.setState({ item: item });     
        this.recalcularEquiposYEspacios();
    }

    prepareOptionsEquipos(items) {
        let options = [];
        for (let i = 0; i < items.length; i++) {
            let text = items[i].nombre + " - " + items[i].descripcion + " - " + items[i].marca.descripcion;
            options.push({ value: items[i].id, label: text, activo: items[i].activo });
        }
        return options;
    }

    prepareOptionsEspacio(items) {
        let options = [];
        for (let i = 0; i < items.length; i++) {
            let text = items[i].nombre;
            options.push({ value: items[i].id, label: text, activo: items[i].activo });
        }
        return options;
    }

    handleEquipos(items) {
        const { equipos } = this.state;
        let selected = [];
        for (let e = 0; e < items.length; e++) {
            for (let i = 0; i < equipos.length; i++) {
                if (items[e].value === equipos[i].id) {
                    selected.push(equipos[i]);
                }
            }
        }
        let { item } = this.state;
        item.equipo = selected;
        this.setState({ item: item });
        this.recalcularEquiposYEspacios();
    }

    handleEspacios(espacio) {
        const { espacios } = this.state;
        let selected;
        for (let i = 0; i < espacios.length; i++) {
            if (espacio.value === espacios[i].id) {
                selected = espacios[i];
                break;
            }
        }
        let { item } = this.state;
        item.espacio = selected;
        this.setState({ item: item });
        this.recalcularEquiposYEspacios();
    }

    async handleSubmit(event) {
        event.preventDefault();
        const { equiposEnUso, espacioEnUso } = this.state;
      
        if (equiposEnUso.length === 0 && espacioEnUso === null) {
            const { item } = this.state;
            
            if (item.user === null) {
                NotificationManager.error('Error en el usuario.', 'Error en los datos', 5000, () => {
                    alert('callback');
                });
                return;
            } 

            if (item.espacio === null && item.equipo.length === 0) {
                NotificationManager.error('Debe realizar por lo menos una reserva.', 'Error en los datos', 5000, () => {
                    alert('callback');
                });
                return;
            }

            if (item.desde > item.hasta) {
                NotificationManager.error('La hora de inicio no puede ser posterior a la hora de fin.', 'Error en los datos', 5000, () => {
                    alert('callback');
                });
                return;
            }

            if (parseInt(item.desde) < 8 || parseInt(item.hasta <= 8) || item.desde === item.hasta) {
                NotificationManager.error('No se pueden realizar reservas en el rango horario especificado.', 'Error en los datos', 5000, () => {
                    alert('callback');
                });
                return;
            }

            item.dia = item.fecha;
            await fetch('/reservas', {
                method: (item.id) ? 'PUT' : 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item),
            });
            this.props.history.push('/reservar');
        }
        NotificationManager.error('Existe al menos un equipo/espacio ocupado en el horario elegido!', 'Error en los datos', 5000, () => {
           alert('callback');
        });

        this.recalcularEquiposYEspacios();
        return;
    }

    render() {

        const { item, equipos, espacios, espacioEnUso, equiposEnUso } = this.state;
        const title = <h2>{item.idReserva ? 'Editar reserva' : 'Crear reserva'}</h2>;
        const optionsEspacio = this.prepareOptionsEspacio(espacios);
        const optionsEquipo = this.prepareOptionsEquipos(equipos);
        
        const horarios = ['08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'],
        llenar = function(hora) {
            return <option>{hora}</option>;
        };

        let selectedEspacio = null;
        if (item.espacio !== null && item.espacio !== undefined)
            selectedEspacio = { value: item.espacio.id, label: item.espacio.nombre };
        
        let selectedEquipos = null;
        if (item.equipo !== null && item.equipo !== undefined)
            selectedEquipos = this.prepareOptionsEquipos(item.equipo);

        const alertEspacioEnUso = espacioEnUso === null ? null : <Alert color="danger">El espacio {espacioEnUso.descripcion} se encuentra en ocupado en la fecha/hora seleccionadas.</Alert>;

        let alertEquiposEnUso = null;
        if (!(equiposEnUso.length === 0)) {
            let nombres = '';
            for (let i = 0; i < equiposEnUso.length; i++) {
                nombres = nombres === '' ? nombres + equiposEnUso[i].nombre : nombres + ", " + equiposEnUso[i].nombre;
            }
            alertEquiposEnUso = <Alert color="danger">Los equipos que usted intenta reservar: {nombres} Se encuentran en uso en la fecha/horas seleccionadas.</Alert>;
        }

        if (this.state.role === 'ADMIN' || this.state.user.name === item.user.name) {
            return <div>
                <AppNavbar />
                <Container>
                    {title}
                    <Form id='form' onSubmit={this.handleSubmit} >
                        <Row>
                            <FormGroup className="col-md-4 mb-3">
                                <Label for="fecha">Fecha</Label>
                                <Input type="date" name="fecha" id="fecha" onChange={this.handleChange} placeholder="Fecha" value={item.fecha} required />
                            </FormGroup>
                            <FormGroup className="col-md-4 mb-3">
                                <Label for="desde">H. Inicio</Label>
                                <CustomInput type="select" id="desde" name="desde" onChange={this.handleChange} value={item.desde.split(':')[0]} required>
                                {horarios.map(llenar)}
                                </CustomInput>
                            </FormGroup>
                            <FormGroup className="col-md-4 mb-3">
                                <Label for="hasta">H. Fin</Label>
                                <CustomInput type="select" id="hasta" name="hasta" onChange={this.handleChange} value={item.hasta.split(':')[0]} required>
                                {horarios.map(llenar)}
                                </CustomInput>
                            </FormGroup>
                        </Row>
                        <FormGroup>
                            <Label for="equipo">Espacio</Label>
                            <Select
                                placeholder="Espacios..."
                                value={selectedEspacio}
                                onChange={this.handleEspacios}
                                options={optionsEspacio}
                                isMulti={false}
                                isOptionDisabled={(option) => option.activo === false}
                            />
                            <FormText>Los espacios no seleccionables se encuentran ocupados en este horario</FormText>
                        </FormGroup>
                        <FormGroup>
                            <Label for="equipo">Equipos</Label>
                            <Select
                                placeholder="Equipos..."
                                value={selectedEquipos}
                                onChange={this.handleEquipos}
                                options={optionsEquipo}
                                isMulti={true}
                                isOptionDisabled={(option) => option.activo === false}
                            />
                            <FormText>Los equipos no seleccionables se encuentran ocupados en este horario</FormText>
                        </FormGroup>
                        <FormGroup>
                        </FormGroup>
                        <br />
                        <FormGroup>
                            {alertEspacioEnUso}
                            {alertEquiposEnUso}
                        </FormGroup>
                        <FormGroup className="col-md-4 mb-3">
                            <Button color="primary" type="submit">Guardar</Button>{' '}
                            <Button color="secondary" tag={Link} to="/reservar">Cancelar</Button>
                        </FormGroup>
                    </Form>
                    <NotificationContainer/>
                </Container >
            </div >
        }
        else {
            return <Redirect to='/' />
        }  
    }
    
}
export default withRouter(ReservarEdit);