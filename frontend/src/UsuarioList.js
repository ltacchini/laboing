import React, { Component } from 'react';
import { Button, ButtonGroup, Container} from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link , Redirect} from 'react-router-dom';
import matchSorter from 'match-sorter'
import ReactTable from "react-table";
import "react-table/react-table.css";

class UsuarioList extends Component {
    //<td className="text-center">{usuario.rol[0].role}</td>
    constructor(props) {
        super(props);
        let rol;
        const cachedUser = localStorage.getItem('user');
        try {
            rol = JSON.parse(cachedUser).roles[0].role;
          } 
        catch (e) {
            this.state = { isOpen: false, isLoading: false, role: '' };
        }
        this.state = { usuario: [], isLoading: true,  role: rol };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        fetch('$2y$12$6KfOZEfpbKUQtZQzgdGkmuli79dafr0HfcRhUdXC0F888vL3yEIFG')
            .then(response => response.json())
            .then(data => this.setState({ usuario: data, isLoading: false }));
    }

    async remove(id) {
        console.log(id)
        await fetch(`/usuarios/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(() => {
            let updatedusuario = [...this.state.usuario].filter(i => i.id !== id);
            this.setState({ usuario: updatedusuario });
        });
    }

    render() {
        const { usuario, isLoading} = this.state;

        if (isLoading) {
            return <p>Cargando...</p>;
        }
        if (this.state.role === 'ADMIN') {
            return (
            <div>
                <AppNavbar />
                <Container fluid>
                    <div className="float-right">
                        <Button color="info" tag={Link} to="/usuario/new">Agregar usuario</Button>
                    </div>
                    <h3>Usuarios</h3>
                    <ReactTable
                        previousText={'Pág. previa'}
                        nextText={'Pág. siguiente'}
                        pageText={'Página'}
                        rowsText={'filas'}
                        ofText={'de'}
                        
                        data={usuario}
                        filterable
                        defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                        columns={[
                            {
                            columns: [
                                {                          
                                    Header: "Email",
                                    id: "email",
                                    accessor: d => d.email,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["email"] }),
                                    filterAll: true
                                },
                                {                          
                                    Header: "Nombre de usuario",
                                    id: "nombre",
                                    accessor: d => d.name,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["nombre"] }),
                                    filterAll: true
                                },
                                {                          
                                    Header: "Nombre",
                                    id: "firstName",
                                    accessor: d => d.firstName,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["firstName"] }),
                                    filterAll: true
                                },
                                {                          
                                    Header: "Apellido",
                                    id: "apellido",
                                    accessor: d => d.lastName,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["apellido"] }),
                                    filterAll: true
                                },
                                {
                                    Header: "Rol",
                                    id: "roleId",
                                    accessor: d => d.roles[0].role,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["roleId"] }),
                                    filterAll: true                                                                     
                                }, 
                                {
                                    Header: "Estado",
                                    accessor: 'active',
                                    id: 'active',
                                    Cell: ({ value }) => (value === 1 ? "Habilitado" : "Desabilitado"),
                                    filterable: false
                                },

                                    // filterMethod: (filter, row) => {
                                    //     if (filter.value === "all") {
                                    //         return true;
                                    //     }
                                    //     else if (filter.value === 1) {
                                    //         return row[filter.id] === 1;
                                    //     }
                                    //     else{
                                    //         return row[filter.id] === 0;
                                    //     }
                                    // },
                                    // Filter: ({ filter, onChange }) =>
                                    //     <select
                                    //         onChange={event => onChange(event.target.value)}
                                    //         style={{ width: "100%" }}
                                    //         value={filter ? filter.value : "all"}
                                    //     >
                                    //     <option value="all">Mostrar todos</option>
                                    //     <option value="true">Activo</option>
                                    //     <option value="false">Inactivo</option>
                                    //     </select>
                                    // },               
                                {
                                    Header: 'Acciones',
                                    id: "id",
                                    filterable: false,
                                    accessor: d => d.id,
                                    Cell: ({value}) => (
                                            <div>
                                            <td>
                                                <ButtonGroup>
                                                <Button size="sm" color="info" tag={Link} to={"/usuario/" + value}>Editar</Button>
                                                <Button size="sm" color="info" onClick={() => this.remove(value)}>Borrar</Button>
                                                </ButtonGroup>  
                                                </td>
                                            </div>
                                        )
                                },
                            ]} ,
                        ]} 
                        defaultPageSize={10}
                        className="-striped -highlight"
                    /> 
                </Container>
                <footer class="page-footer font-small blue">
                <div class="footer-copyright text-center py-3">© 2019:
                    Lic. en Sistemas - IDEI - UNGS
                </div>
            </footer>
            </div> 
        );} else {
            return <Redirect to='/' />
          }
    }
}

export default UsuarioList;