import React, { Component } from 'react';
import AppNavbar from './AppNavbar';
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar-reactwrapper/dist/css/fullcalendar.min.css';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { reserva: [], isLoading: true, user: '', events: [] };
    const cachedUser = localStorage.getItem('user');
    this.state.user = JSON.parse(cachedUser);
}

componentDidMount() {
    this.setState({ isLoading: true });
    fetch('/reservas')
        .then(response => response.json())
        .then(data => this.setState({ reserva: data, isLoading: false }));
}

render() {
    const { reserva, isLoading, user } = this.state;
    
    if (isLoading || user === '') {
        return <p>Cargando...</p>;
    }
    var eventList = [];

    reserva.map(reserva => {   
        if (reserva.espacio != null){
            eventList.push(
                {title: reserva.espacio.nombre,
                start: reserva.fecha + 'T' + reserva.desde,
                end: reserva.fecha + 'T' + reserva.hasta,
                color: '#ff5733'}); 
        }        
        else{
            reserva.equipo.forEach(function(element) {
                eventList.push(
                    {title: element.nombre,
                    start: reserva.fecha + 'T' + reserva.desde,
                    end: reserva.fecha + 'T' + reserva.hasta}); 
            });            
        }     
    });
    this.state.events = eventList;

    return (
        <div>
            <AppNavbar />
            <FullCalendar
                id = "your-custom-ID"
                header = {{
                    left: 'prev,next today myCustomButton',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay',
                }}
                monthNames = {['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']}
                dayNames = {['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']}
                dayNamesShort = {['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab']}
                monthNamesShort = {['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic']}
                buttonText = {{
                    today:    'Hoy',
                    month:    'Mes',
                    week:     'Semana',
                    day:      'Día',
                }}

                eventLimitText = 'más'

                navLinks = {true} // can click day/week names to navigate views
                editable = {false}
                allDay = {true}
                aspectRatio = {1.8}
                allDaySlot = {false}
                eventLimit = {true} // allow "more" link when too many events
                //axisFormat = 'H:mm'
                //agenda = 'H:mm{ - h:mm}'
                timeFormat = 'H:mm'
                events = {this.state.events}	
            />
            <footer class="page-footer font-small blue">
                <div class="footer-copyright text-center py-3">© 2019:
                    Lic. en Sistemas - IDEI - UNGS
                </div>
            </footer>
        </div>
              
    );
  }
}

export default Home;