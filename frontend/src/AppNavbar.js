import React, { Component } from 'react';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';

export default class AppNavbar extends Component {

  constructor(props) {
    super(props);
    const cachedUser = localStorage.getItem('user');
    let rol;
    try {
      rol = JSON.parse(cachedUser).roles[0].role;
    } catch (e) {
      this.state = { isOpen: false, isLoading: false, role: '' };
    }
    this.state = { isOpen: false, isLoading: false, role: rol };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    if (this.state.isLoading) {
      return <p>Cargando...</p>;
    } else {
      if (this.state.role === 'ADMIN') {
        return (<Navbar color="dark" dark expand="md">
          <img src={require('./ungs.png')} style={{ width: `200px`, margin: ".5em auto .3em" }} alt="Logo de la UNGS" />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavbarBrand tag={Link} to="/">Inicio</NavbarBrand>
              <NavbarToggler onClick={this.toggle} />
              <NavbarBrand tag={Link} to="/reservar">Reservas</NavbarBrand>
              <NavbarToggler onClick={this.toggle}/>
              <NavbarBrand tag={Link} to="/espacio">Espacios</NavbarBrand>
              <NavbarToggler onClick={this.toggle}/>
              <NavbarBrand tag={Link} to="/equipo">Equipos</NavbarBrand>
              <NavbarToggler onClick={this.toggle}/>
              <NavbarBrand tag={Link} to="/usuario">Usuarios</NavbarBrand>
              <NavbarToggler onClick={this.toggle}/>
              <NavbarBrand tag={Link} to="/logout">Cerrar sesión</NavbarBrand>
              <NavbarToggler onClick={this.toggle}/>
            </Nav>
          </Collapse>
        </Navbar>
        );
      } else if (this.state.role === 'DOCENTE') {
        return (<Navbar color="dark" dark expand="md">
          <img src={require('./ungs.png')} style={{ width: `200px`, margin: ".5em auto .3em" }} alt="Logo de la UNGS" />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
            <NavbarBrand tag={Link} to="/">Inicio</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <NavbarBrand tag={Link} to="/reservar">Reservas</NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
            <NavbarBrand tag={Link} to="/logout">Cerrar sesión</NavbarBrand>
            <NavbarToggler onClick={this.toggle}/>
            </Nav>
          </Collapse>
        </Navbar>);
      } else {
        return <Redirect to='/login' />
      }
    }
  }
}

