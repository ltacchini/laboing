import React, { Component } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import { Button, ButtonGroup, Container, Form, FormText, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';

class GroupEdit extends Component {

  emptyItem = {
    codigo: '',
    nombre: '',
    descripcion: '',
    cantidad: '',
    activo: false
  };
  constructor(props) {
    super(props);
    let rol;
    const cachedUser = localStorage.getItem('user');
    try {
      rol = JSON.parse(cachedUser).roles[0].role;
    } 
    catch (e) {
      this.state = { isOpen: false, isLoading: false, role: '' };
    }
    this.state = {
      item: this.emptyItem,
      role: rol
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.params.id !== 'new') {
      const group = await (await fetch(`/espacios/${this.props.match.params.id}`)).json();
      this.setState({ item: group });
    }
  }

  handleRadioChange(value) {
    let item = { ...this.state.item };
    item.activo = value;
    this.setState({ item });
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = { ...this.state.item };
    item[name] = value;
    this.setState({ item });

    const save = document.getElementById('save');
    const cant = document.getElementById('cantidad');
    save.disabled = cant.value === '' || isNaN(cant.value);
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;

    await fetch('/espacios', {
      method: (item.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/espacio');
  }

  render() {
    const { item } = this.state;
    const title = <h2>{item.id ? 'Editar espacio' : 'Agregar espacio'}</h2>;
    if (this.state.role === 'ADMIN') {
      return <div>
          <AppNavbar />
          <Container>
            {title}
            <Form onSubmit={this.handleSubmit}>
              <FormGroup>
                <Label for="codigo">Codigo</Label>
                <Input type="text" maxLength="50" name="codigo" id="codigo" placeholder="Código" value={item.codigo || ''}
                  onChange={this.handleChange} autoComplete="codigo" required />
              </FormGroup>
              <FormGroup>
                <Label for="nombre">Nombre</Label>
                <Input type="text" maxLength="100" name="nombre" id="nombre" placeholder="Nombre" value={item.nombre || ''}
                  onChange={this.handleChange} required />
              </FormGroup>
              <FormGroup>
                <Label for="descripcion">Descripcion</Label>
                <Input type="text" maxLength="250" name="descripcion" id="descripcion" placeholder="Descripción" value={item.descripcion || ''}
                  onChange={this.handleChange} required />
              </FormGroup>
              <div className="row">
                <FormGroup className="col-md-4 mb-3">
                  <Label for="cantidad">Capacidad</Label>
                  <Input type="text" maxLength="5" name="cantidad" id="cantidad" placeholder="Cantidad" value={item.cantidad || ''}
                    onChange={this.handleChange} required />
                  <FormText>La cantidad debe ser un número</FormText>
                </FormGroup>
              </div>
              <Label for="Activo">Habilitado</Label>
              <div className="row">
                <ButtonGroup className="col-md-4 mb-3">
                  <Button color="primary" name="activo" onClick={() => this.handleRadioChange(true)} active={item.activo === true || item.activo === 1}>Activo</Button>
                  <Button color="primary" name="activo" onClick={() => this.handleRadioChange(false)} active={item.activo === false || item.activo === 0}>Inactivo</Button>
                </ButtonGroup>
              </div>
              <div className="row">
                <FormGroup className="col-md-4 mb-3">
                  <Button id="save" color="primary" type="submit">Guardar</Button>{' '}
                  <Button color="secondary" tag={Link} to="/espacio">Cancelar</Button>
                </FormGroup>
              </div>
            </Form>
          </Container>
        </div>
    }
    else{
      return <Redirect to='/' />
    }
  }
}

export default withRouter(GroupEdit);