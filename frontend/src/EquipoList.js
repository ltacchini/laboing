import React, { Component } from 'react';
import { Button, ButtonGroup, Container} from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link, Redirect } from 'react-router-dom';
import matchSorter from 'match-sorter'
import ReactTable from "react-table";
import "react-table/react-table.css";

class EquipoList extends Component {

    constructor(props) {
        super(props);
        let rol;
        const cachedUser = localStorage.getItem('user');
        try {
            rol = JSON.parse(cachedUser).roles[0].role;
        } 
        catch (e) {
            this.state = { isOpen: false, isLoading: false, role: '' };
    }
        this.state = { equipo: [], isLoading: true, role: rol };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        fetch('/$2y$12$7Ga4okJRF9E/PJPGN0UR..9WfPuhlhRz426VfqxwdWRy2VX8sojYm')
            .then(response => response.json())
            .then(data => this.setState({ equipo: data, isLoading: false }));
    }

    async remove(id) {
        await fetch(`/equipos/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(() => {
            let updatedequipo = [...this.state.equipo].filter(i => i.id !== id);
            this.setState({ equipo: updatedequipo });
        });
    }
    

    render() {
        const { equipo, isLoading } = this.state;

        if (isLoading) {
            return <p>Cargando...</p>;
        }
        if (this.state.role === 'ADMIN') {
            return (
                <div>
                    <AppNavbar />
                    <Container fluid>
                        <div className="float-right">
                            <Button color="info" tag={Link} to="/equipo/new">Agregar equipo</Button>
                        </div>
                        <h3>Equipos</h3>

                        <ReactTable
                            previousText={'Pág. previa'}
                            nextText={'Pág. siguiente'}
                            pageText={'Página'}
                            rowsText={'filas'}
                            ofText={'de'}
                            
                            data={equipo}
                            filterable
                            defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                            columns={[
                                {
                                columns: [
                                    {                          
                                        Header: "Número de Inventario",
                                        id: "nro_inventario",
                                        accessor: d => d.nro_inventario,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["nro_inventario"] }),
                                        filterAll: true
                                    },
                                    {                          
                                        Header: "Nombre",
                                        id: "nombre",
                                        accessor: d => d.nombre,
                                        style:{ 'whiteSpace': 'unset'},
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["nombre"] }),
                                        filterAll: true
                                    },
                                    {
                                        Header: "Descripción",
                                        id: "descripcion",
                                        style:{ 'whiteSpace': 'unset'},
                                        accessor: d => d.descripcion,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["descripcion"] }),
                                        filterAll: true
                                    },
                                    {
                                        Header: "Marca",
                                        id: "marca",
                                        accessor: d => d.marca.descripcion,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["marca"] }),
                                        filterAll: true
                                    },
                                    {
                                        Header: "Tipo",
                                        id: "tipo",
                                        style:{ 'whiteSpace': 'unset'},
                                        accessor: d => d.tipo.descripcion,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["tipo"] }),
                                        filterAll: true
                                    },
                                    {
                                    Header: "Estado",
                                    accessor: "activo",
                                    id: "activo",
                                    Cell: ({ value }) => (value === true ? "Activo" : "Inactivo"),
                                    filterMethod: (filter, row) => {
                                        if (filter.value === "all") {
                                        return true;
                                        }
                                        if (filter.value === "true") {
                                            return row[filter.id] === true;
                                        }
                                            return row[filter.id] === false;
                                        },
                                    Filter: ({ filter, onChange }) =>
                                        <select
                                            onChange={event => onChange(event.target.value)}
                                            style={{ width: "100%" }}
                                            value={filter ? filter.value : "all"}
                                        >
                                        <option value="all">Mostrar todos</option>
                                        <option value="true">Activo</option>
                                        <option value="false">Inactivo</option>
                                        </select>
                                    },
                                    {
                                    Header: 'Acciones',
                                    id: "id",
                                    filterable: false,
                                    accessor: d => d.id,
                                    Cell: ({value}) => (
                                            <div>
                                                <td>
                                                <ButtonGroup>
                                                <Button size="sm" color="info" tag={Link} to={"/equipo/" + value}>Editar</Button>
                                                <Button size="sm" color="info" onClick={() => this.remove(value)}>Borrar</Button>
                                                </ButtonGroup>  
                                                </td>
                                            </div>
                                        )
                                    },
                                ]},
                            ]}
                            defaultPageSize={10}
                            className="-striped -highlight"                   
                        />               
                        </Container>  
                    <div class="footer-copyright text-center py-3">© 2019:
                        Lic. en Sistemas - IDEI - UNGS
                    </div>
                </div>
            );
        } 
        else{
            return <Redirect to='/'/>
        }
    }
}

export default EquipoList;