import React, { Component } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import { Button, ButtonGroup, Container, Form, FormGroup, Input, Label, CustomInput } from 'reactstrap';
import AppNavbar from './AppNavbar';

class GroupEdit extends Component {

  emptyItem = {
    email: '',
    name: '',
    password: '',
    firstName: '',
    lastName: '',
    active: 0,
    'roles': [{
      'roleId': '1',
      'role': 'ADMIN'
    }]
  };
  constructor(props) {
    super(props);

    let rol;
    const cachedUser = localStorage.getItem('user');
    try {
      console.log(cachedUser);
      rol = JSON.parse(cachedUser).roles[0].role;
      } catch (e) {
        this.state = { isOpen: false, isLoading: false, role: '' };
      }
    
    this.state = {
      item: this.emptyItem, role: rol 
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.params.id !== 'new') {
      const group = await (await fetch(`/usuarios/${this.props.match.params.id}`)).json();
      this.setState({ item: group });
    }
  }

  handleRadioChange(value) {
    let item = { ...this.state.item };
    item.active = value;
    console.log(item);
    this.setState({ item });
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let item = { ...this.state.item };
    switch (name) {
      case 'roles':
        const options = target.options;
        item[name] = [{
          'roleId': value,
          'role': options[options.selectedIndex].text
        }]
        break;
      case 'active':
        const checked = target.checked;
        item[name] = checked ? '1' : '0';
        break;
      default:
        item[name] = value;
    }
    console.log(item);
    this.setState({ item });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;

    console.log(item);
    await fetch('/usuarios', {
      method: (item.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/usuario');
  }

  render() {
    const { item } = this.state;
    const title = <h2>{item.id ? 'Editar usuario' : 'Agregar usuario'}</h2>;
    
    if (this.state.role === 'ADMIN') {
      return <div>
        <AppNavbar />
        <Container>
          {title}
          <Form onSubmit={this.handleSubmit}>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input type="text" maxLength="36" name="email" id="email" placeholder="Email" value={item.email || ''}
                onChange={this.handleChange} required />
            </FormGroup>
            <FormGroup>
              <Label for="nombre">Nombre</Label>
              <Input type="text" maxLength="16" name="firstName" id="firstName" placeholder="Nombre" value={item.firstName || ''}
                onChange={this.handleChange} required/>
            </FormGroup>
            <FormGroup>
              <Label for="lastName">Apellido</Label>
              <Input type="text" maxLength="16" name="lastName" id="lastName" placeholder="Apellido" value={item.lastName || ''}
                onChange={this.handleChange} required />
            </FormGroup>
            <FormGroup>
              <Label for="nombre">Nombre de usuario</Label>
              <Input type="text" maxLength="16" name="name" id="name" placeholder="Nombre de usuario" value={item.name || ''}
                onChange={this.handleChange} required/>
            </FormGroup>
              <FormGroup >
                <Label for="password">Contraseña</Label>
                <Input type="password" maxLength="16" name="password" id="password" placeholder="Contraseña" onChange={this.handleChange} value={item.password || ''} required />
              </FormGroup>
            <Label for="active">Habilitado</Label>
            <div className="row">
              <ButtonGroup className="col-md-4 mb-3">
                <Button color="primary" name="active" onClick={() => this.handleRadioChange(1)} active={item.active === 1}>Activo</Button>
                <Button color="primary" name="active" onClick={() => this.handleRadioChange(0)} active={item.active === 0}>Inactivo</Button>
              </ButtonGroup>
            </div>
            <FormGroup >
              <FormGroup>
                <Label for="roles">Rol</Label>
                <CustomInput type="select" id="roles" name="roles" onChange={this.handleChange} value={item.roles[0].roleId || 1}>
                  <option value="1">ADMIN</option>
                  <option value="2">DOCENTE</option>
                </CustomInput>
              </FormGroup>
            </FormGroup>
            <FormGroup>
              <Button color="primary" type="submit">Guardar</Button>{' '}
              <Button color="secondary" tag={Link} to="/usuario">Cancelar</Button>
            </FormGroup>
          </Form>
        </Container>
      </div>
    }
    else {
      return <Redirect to='/' />
    }
  }
}

export default withRouter(GroupEdit);