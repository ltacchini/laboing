import React, { Component } from 'react';
import { Button, ButtonGroup, Container} from 'reactstrap';
import AppNavbar from './AppNavbar';
import ModalExample from './ModalExample'
import { Link } from 'react-router-dom';
import matchSorter from 'match-sorter'
import ReactTable from "react-table";
import "react-table/react-table.css";

class ReservarList extends Component {

    constructor(props) {
        super(props);
        this.state = { reserva: [], isLoading: true, user: ''};
        this.remove = this.remove.bind(this);
        const cachedUser = localStorage.getItem('user');
        this.state.user = JSON.parse(cachedUser);
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        fetch('/reservas')
            .then(response => response.json())
            .then(data => this.setState({ reserva: data, isLoading: false }));
    }

    async remove(id) {
        await fetch(`/reservas/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(() => {
            let updatedreserva = [...this.state.reserva].filter(i => i.idReserva !== id);
            this.setState({ reserva: updatedreserva });
        });
    }

    render() {
        const { reserva, isLoading, user } = this.state;
        let role;

        if (isLoading || user === '') {
            return <p>Cargando...</p>;
        }

        reserva.map(reserva => {                     
            let buttons;
            if (user !== undefined && reserva !== undefined) {
                role = user.roles[0].role;
                if (role === 'ADMIN' || (user.name === reserva.user.name)) {
                    buttons = <ButtonGroup>
                        <Button size="sm" color="info" tag={Link} to={"/reservar/" + reserva.idReserva}>Editar</Button>
                        <Button size="sm" color="info" onClick={() => this.remove(reserva.idReserva)}>Borrar</Button>
                    </ButtonGroup>;
                } else {
                    buttons = '';
                }
            } else {
                buttons = '';
            }
            return <tr key={reserva.idReserva}>
                <td>
                    {buttons}
                </td>
            </tr>
        });

        return (
            <div>
                <AppNavbar />
                <Container fluid>
                    <div className="float-right">
                        <Button color="info" tag={Link} to="/reservar/new">Agregar reserva</Button>
                    </div>
                    <h3>Reservas</h3>
                    <ReactTable
                        data={reserva}
                        previousText={'Pág. previa'}
                        nextText={'Pág. siguiente'}
                        pageText={'Página'}
                        rowsText={'filas'}
                        ofText={'de'}
                        filterable
                        defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                        columns={[{
                            columns: [
                                {                          
                                    Header: "Usuario",
                                    id: "usuario",
                                    accessor: d => d.user.name ,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["usuario"] }),
                                    filterAll: true   
                                },
                                {                          
                                    Header: "Fecha",
                                    id: "fecha",
                                    accessor: d => d.fecha,
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["fecha"] }),
                                    filterAll: true
                                },
                                {                          
                                    Header: "Desde",
                                    id: "desde",
                                    accessor: d => d.desde.slice(-8, 5),
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["desde"] }),
                                    filterAll: true
                                },
                                {                          
                                    Header: "Hasta",
                                    id: "hasta",
                                    accessor: d => d.hasta.slice(-8, 5),
                                    filterMethod: (filter, rows) =>
                                    matchSorter(rows, filter.value, { keys: ["hasta"] }),
                                    filterAll: true
                                },
                                {
                                    Header: "Estado",
                                    accessor: "activo",
                                    id: "activo",
                                    Cell: ({ value }) => (value === true ? "Reservado" : "No reservado"),
                                    filterMethod: (filter, row) => {
                                        if (filter.value === "all") {
                                           return true;
                                        }
                                        if (filter.value === "true") {
                                            return row[filter.id] === true;
                                        }
                                            return row[filter.id] === false;
                                        },
                                       
                                    Filter: ({ filter, onChange }) =>
                                        <select
                                            onChange={event => onChange(event.target.value)}
                                            style={{ width: "100%" }}
                                            value={filter ? filter.value : "all"}
                                        >
                                        <option value="all">Mostrar todos</option>
                                        <option value="true">Reservado</option>
                                        <option value="false">No reservado</option>
                                        </select>
                                    },    
                                    {                    
                                        Header: "Espacio",
                                        id: "espacio",
                                        style:{ 'whiteSpace': 'unset'},
                                        accessor: d => {   
                                            if (d.espacio == null){
                                                return "-"
                                            }
                                            return d.espacio.nombre
                                        },
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["espacio"] }),
                                        filterAll: true
                                    }, 
                                    {
                                        Header: 'Equipos',
                                        id: "row",
                                        accessor: d => d.index,
                                        filterable: false,
                                        Cell: (row) => (
                                            <div>
                                                <ModalExample color='info' buttonLabel='Ver equipos' equipos={reserva[row.index].equipo}></ModalExample>
                                            </div>)                                            
                                    },                           
                                    {
                                    Header: 'Acciones',
                                    id: "row",
                                    filterable: false,
                                    accessor: d => d.index,
                                        Cell: (row) => { 
                                        if (role === 'ADMIN' || (user.name === reserva[row.index].user.name)) {
                                           return  <ButtonGroup>
                                                <Button size="sm" color="info" tag={Link} to={"/reservar/" + reserva[row.index].idReserva}>Editar</Button>
                                                <Button size="sm" color="info" onClick={() => this.remove(reserva[row.index].idReserva)}>Borrar</Button>
                                            </ButtonGroup>;
                                        } else {
                                            return '';
                                        }}                                        
                                    }
                            ]},
                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                    />    
                </Container>
                <div class="footer-copyright text-center py-3">© 2019:
                    Lic. en Sistemas - IDEI - UNGS
                </div>
            </div>
        );
    }
}

export default ReservarList;