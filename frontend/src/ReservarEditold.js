import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button, Container, Row, CustomInput, Input, Form, FormGroup, Label, ListGroup, ListGroupItem } from 'reactstrap';
import AppNavbar from './AppNavbar';

class ReservarEdit extends Component {

    emptyItem = {
        'idReserva': 0,
        'fecha': '2018-01-01',
        'desde': '08:00:00',
        'hasta': '09:00:00',
        'activo': true,
        'user': '',
        'espacio': null,
        'equipo': [],
    };

    constructor(props) {
        super(props);
        const cachedUser = localStorage.getItem('user');
        this.state = {
            user: JSON.parse(cachedUser),
            item: this.emptyItem,
            equipos: [],
            espacios: [],
            reservas: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleRemove = this.handleRemove.bind(this);
    }

    async componentDidMount() {
        if (this.props.match.params.id !== 'new') {
            const group = await (await fetch(`/reservas/${this.props.match.params.id}`)).json();
            this.setState({ item: group });
        } else {
            let d = new Date();
            let item = this.state.item
            let mes = d.getMonth() < 10 ? '0' + d.getMonth().toString() : d.getMonth();
            let dia = d.getDate() < 10 ? '0' + d.getDate().toString() : d.getDate();
            item['fecha'] = d.getFullYear() + '-' + mes + '-' + dia;
            const cachedUser = localStorage.getItem('user');
            item.user = JSON.parse(cachedUser);
            this.setState({ item: item });
        }
        this.setEquipos();
        this.setEspacios();
    }

    setEspacios() {
        const { item } = this.state;
        let body = JSON.stringify(item);
        fetch('/reservas/espacios', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: body,
        })
            .then(response => response.json())
            .then(data => this.setEspaciosOpt(data));
    }

    setEspaciosOpt(data) {
        let opt = [];
        data = data.filter(function (espacio) {
            return espacio.activo;
        });
        opt.push(<option key='-1' value='-1'> - </option>);
        for (let i = 0; i < data.length; i++) {
            opt.push(<option key={data[i].id} value={data[i].id}> {data[i].nombre} </option>);
        }
        this.setState({ espacioOpt: opt, espacios: data });
    }

    setEquipos() {
        const { item } = this.state;
        fetch('/reservas/equipos', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        })
            .then(response => response.json())
            .then(data => this.setEquiposOpt(data));
    }

    setEquiposOpt(data) {
        let opt = [];
        let descripcion;
        data = data.filter(function (equipo) {
            return equipo.activo;
        });

        for (let i = 0; i < data.length; i++) {
            descripcion = data[i].nombre + ' - ' + data[i].descripcion + ' - ' + data[i].marca.descripcion;
            opt.push(<option key={data[i].id} value={data[i].id}> {descripcion} </option>);
        }
        this.setState({ equipoOpt: opt, equipos: data });
    }

    handleRadioChange(value) {
        let item = { ...this.state.item };
        item.activo = value;
        this.setState({ item });
    }

    handleRemove() {
        let item = { ...this.state.item };
        if (this.state.equipoSel === '') { }
        else {
            const idSel = this.state.equipoSel.id;
            item['equipo'] = item['equipo'].filter(function (equipo) {
                return equipo.id !== idSel;
            });
        }
        this.setState({ item: item });
    }

    handleAdd() {
        let item = { ...this.state.item };
        if (this.state.equipoSel === '') {
            item['equipo'].push(this.state.equipos[0]);
        } else {
            item['equipo'].push(this.state.equipoSel);
        }
        this.setState({ item: item });
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        let item = { ...this.state.item };
        let equipo = this.state.equipo;
        let espacio = this.state.espacio;
        let desde = item.desde.split(':')[0];
        let hasta = item.hasta.split(':')[0];
        switch (name) {
            case 'equipo':
                let equipos = this.state.equipos;
                for (let i = 0; i < this.state.equipos.length; i++) {
                    if (equipos[i].id.toString() === value.toString()) {
                        equipo = equipos[i];
                    }
                }
                break;
            case 'espacio':
                let espacios = this.state.espacios;
                for (let i = 0; i < this.state.espacios.length; i++) {
                    if (espacios[i].id.toString() === value.toString()) {
                        espacio = espacios[i];
                        item[name] = espacios[i];
                    }
                    else if (value === '-1') {
                        item[name] = null;
                    }
                }
                break;
            case 'desde':
                let inicio = value.split(':');
                let iDesde = parseInt(inicio[0]);
                if (iDesde < hasta && iDesde > 7) {
                    inicio[1] = '00'
                    inicio = inicio.join(':');
                    item[name] = inicio + ':00';
                    item['equipo'] = [];
                    item['espacio'] = null;
                }
                break;
            case 'hasta':
                let fin = value.split(':');
                let iFin = parseInt(fin[0]);
                if (iFin > desde) {
                    fin[1] = '00'
                    fin = fin.join(':');
                    item[name] = fin + ':00';
                    item['equipo'] = [];
                    item['espacio'] = null;
                }
                break;
            case 'fecha':
                item[name] = value;
                item['equipo'] = [];
                item['espacio'] = null;
                break;
            default:
                item[name] = value;
        }
        this.setState({ item: item, equipoSel: equipo, espacioSel: espacio });
        if (name === 'fecha' || name === 'desde' || name === 'hasta') {
            this.setEquipos();
            this.setEspacios();
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const { item } = this.state;

        await fetch('/reservas', {
            method: (item.id) ? 'PUT' : 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item),
        });
        this.props.history.push('/reservar');
    }

    render() {
        const { item } = this.state;
        const title = <h2>{item.id ? 'Editar reserva' : 'Crear reserva'}</h2>;

        let espacio = ''
        try { espacio = item.espacio.id; } catch (err) { }

        const equiposSel = item.equipo.filter(function (item, pos, arr) { return arr.indexOf(item) === pos; })

        try {
            if (equiposSel.lenght !== item.equipo.lenght) { this.setState({ equipo: equiposSel }); }
        } catch (err) { }

        const EquiposSelList = equiposSel.map(function (equipo) {
            let descripcion = ''
            try { descripcion = equipo.nombre + ' - ' + equipo.descripcion + ' - ' + equipo.marca.descripcion; } catch (err) { }
            return (
                <ListGroupItem key={equipo.id}>
                    {descripcion}
                </ListGroupItem>
            )
        });
        return <div>
            <AppNavbar />
            <Container>
                {title}
                <Form onSubmit={this.handleSubmit}>
                    <Row>
                        <FormGroup className="col-md-4 mb-3">
                            <Label for="fecha">Fecha</Label>
                            <Input type="date" name="fecha" id="fecha" onChange={this.handleChange} placeholder="Fecha" value={item.fecha} required />
                        </FormGroup>
                        <FormGroup className="col-md-4 mb-3">
                            <Label for="desde">H. Inicio</Label>
                            <Input type="time" name="desde" id="desde" onChange={this.handleChange} placeholder="Hora inicio" value={item.desde} required />
                        </FormGroup>
                        <FormGroup className="col-md-4 mb-3">
                            <Label for="hasta">H. Fin</Label>
                            <Input type="time" name="hasta" id="hasta" onChange={this.handleChange} placeholder="Hora inicio" value={item.hasta} required />
                        </FormGroup>
                    </Row>
                    <FormGroup>
                        <Label for="espacio">Espacio</Label>
                        <CustomInput type="select" id="espacio" name="espacio" onChange={this.handleChange} value={espacio}>
                            {this.state.espacioOpt}
                        </CustomInput>
                    </FormGroup>
                    <FormGroup>
                        <Label for="equipo">Equipo</Label>
                        <CustomInput type="select" id="addEspacio" name="equipo" onChange={this.handleChange} value={this.state.equipoOpt.id}>
                            {this.state.equipoOpt}
                        </CustomInput>
                    </FormGroup>
                    <FormGroup className="col-md-4 mb-3">
                        <Button id="addEquipo" color="primary" size="sm" name="equipo" onClick={this.handleAdd}>Agregar</Button>{' '}
                        <Button id="removeEquipo" color="secondary" size="sm" onClick={this.handleRemove}>Quitar</Button>
                    </FormGroup>
                    <FormGroup>
                        <ListGroup>
                            {EquiposSelList}
                        </ListGroup>
                    </FormGroup>
                    <br />
                    <FormGroup className="col-md-4 mb-3">
                        <Button color="primary" type="submit">Guardar</Button>{' '}
                        <Button color="secondary" tag={Link} to="/reservar">Cancelar</Button>
                    </FormGroup>
                </Form>
            </Container >
        </div >
    }
}

export default withRouter(ReservarEdit);