import React, { Component } from 'react';
import { Link, withRouter, Redirect } from 'react-router-dom';
import { Button, ButtonGroup, Container, CustomInput, Form, FormGroup, Input, Label } from 'reactstrap';
import AppNavbar from './AppNavbar';

class EquipoEdit extends Component {

  emptyItem = {
    'id': 0,
    'nombre': '',
    'nro_inventario': '',
    'descripcion': '',
    'activo': false,
    'marca': {
      'id': 1,
      'descripcion': 'ABB'
    },
    'tipo': {
      'idTipo': 1,
      'descripcion': 'Equipo'
    }
  };

  constructor(props) {
    super(props);
    let rol;
    const cachedUser = localStorage.getItem('user');
    try {
        rol = JSON.parse(cachedUser).roles[0].role;
      } 
    catch (e) {
        this.state = { isOpen: false, isLoading: false, role: '' };
    }
    this.state = {
      item: this.emptyItem,
      marcas: [],
      tipos: [], 
      role: rol
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.params.id !== 'new') {
      const group = await (await fetch(`/equipos/${this.props.match.params.id}`)).json();
      this.setState({ item: group });
    }
    fetch('/marcas')
      .then(response => response.json())
      .then(data => this.setMarcas(data));

    fetch('/tipoProductos')
      .then(response => response.json())
      .then(data => this.setTipos(data));
  }

  setTipos(data) {
    let opt = [];
    for (let i = 0; i < data.length; i++) {
      opt.push(<option key={data[i].idTipo} value={data[i].idTipo}> {data[i].descripcion} </option>);
    }
    this.setState({ tipos: opt });
  }

  setMarcas(data) {
    let opt = [];
    for (let i = 0; i < data.length; i++) {
      opt.push(<option key={data[i].id} value={data[i].id}> {data[i].descripcion} </option>);
    }
    this.setState({ marcas: opt });
  }

  handleRadioChange(value) {
    let item = { ...this.state.item };
    item.activo = value;
    this.setState({ item });
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    let options;
    let item = { ...this.state.item };

    switch (name) {
      case 'marca':
        options = target.options;
        item[name] = {
          id: value,
          descripcion: options[options.selectedIndex].text
        };
        break;
      case 'tipo':
        options = target.options;
        item[name] = {
          idTipo: value,
          descripcion: options[options.selectedIndex].text
        };
        break;
      default:
        item[name] = value;
    }
    console.log(item);
    this.setState({ item });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const { item } = this.state;

    await fetch('/equipos', {
      method: (item.id) ? 'PUT' : 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item),
    });
    this.props.history.push('/equipo');
  }

  render() {
    const { item } = this.state;
    const title = <h2>{item.id ? 'Editar equipo' : 'Agregar equipo'}</h2>;

    if (this.state.role === 'ADMIN') {
      return <div>
        <AppNavbar />
        <Container>
          {title}
          <Form onSubmit={this.handleSubmit}>
            <FormGroup>
              <Label for="nombre">Nombre</Label>
              <Input type="text" maxLength="50" name="nombre" id="nombre" placeholder="Nombre" value={item.nombre || ''}
                onChange={this.handleChange} autoComplete="nombre" required />
            </FormGroup>
            <FormGroup>
              <Label for="nombre">Nro. de Inventario</Label>
              <Input type="text" maxLength="50" name="nro_inventario" id="nro_inventario" placeholder="XXXXXXXX" value={item.nro_inventario || ''}
                onChange={this.handleChange} autoComplete="nombre" required />
            </FormGroup>
            <FormGroup>
              <Label for="descripcion">Descripcion</Label>
              <Input type="text" maxLength="50" name="descripcion" id="descripcion" placeholder="Descripción" value={item.descripcion || ''}
                onChange={this.handleChange} autoComplete="descripcion" required />
            </FormGroup>
            <FormGroup>
              <Label for="tipo">Tipo</Label>
              <CustomInput type="select" id="tipo" name="tipo" onChange={this.handleChange} value={item.tipo.idTipo}>
                {this.state.tipos}
              </CustomInput>
            </FormGroup>
            <FormGroup>
              <Label for="marca">Marca</Label>
              <CustomInput type="select" id="marca" name="marca" onChange={this.handleChange} value={item.marca.id}>
                {this.state.marcas}
              </CustomInput>
            </FormGroup>
            <Label for="Activo">Habilitado</Label>
            <div className="row">
              <ButtonGroup className="col-md-4 mb-3">
                <Button color="primary" name="activo" onClick={() => this.handleRadioChange(true)} active={item.activo === true || item.activo === 1}>Activo</Button>
                <Button color="primary" name="activo" onClick={() => this.handleRadioChange(false)} active={item.activo === false || item.activo === 0}>Inactivo</Button>
              </ButtonGroup>
            </div>
            <FormGroup>
              <Button color="primary" type="submit">Guardar</Button>{' '}
              <Button color="secondary" tag={Link} to="/equipo">Cancelar</Button>
            </FormGroup>
          </Form>
        </Container>
      </div>
    }
    else{ 
      return <Redirect to='/' />
    }
  }
}

export default withRouter(EquipoEdit);