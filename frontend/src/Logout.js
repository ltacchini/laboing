import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Logout extends Component {

    constructor(props) {
        super(props);
        localStorage.removeItem('user');
        this.props.history.push('/login');
    }

    render() {
        return <p>Cargando...</p>;
    }
}
export default withRouter(Logout);