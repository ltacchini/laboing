import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import EspacioList from './EspacioList';
import EspacioEdit from './EspacioEdit';
import UsuarioList from './UsuarioList';
import UsuarioEdit from './UsuarioEdit';
import EquipoList from './EquipoList';
import EquipoEdit from './EquipoEdit';
import ReservarList from './ReservarList';
import ReservarEdit from './ReservarEdit';
import Login from './Login';
import Logout from './Logout';

class ProtectedRoute extends Component {

  user = {
    "id": '',
    "email": '',
    "password": '',
    "name": '',
    "lastName": '',
    "active": 1,
    "roles": ''
  }

  constructor(props) {
    super(props);

    this.state = {
      user: this.user,
      authenticated: false,
      isLoading: true
    };
    const cachedUser = localStorage.getItem('user');
    this.verifyUser(cachedUser);
  }

  verifyUser(user) {
    /*
     * user === null ? this.setState({authenticated : false, isLoading:false}) : this.setState({authenthicated: true, isLoading: false});
     */
    user === null ? this.state.authenticated = false : this.state.authenticated = true;
    this.state.isLoading = false;
  }
  
  render() {
    const { component: Component, ...props } = this.props
    if (this.state.isLoading) {
      return <p>Cargando...</p>;
    } else {
      return (
        <Route
        {...props}
        render={props => (
          this.state.authenticated ?
          <Component {...props} /> :
          <Redirect to='/login' />
          )}
        />
      )
    }
  }
}

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <ProtectedRoute path='/' exact={true} component={Home} />
          <Route path='/login' component={Login} />
          <ProtectedRoute path='/logout' component={Logout} />
          <ProtectedRoute path='/espacio' exact={true} component={EspacioList} />
          <ProtectedRoute path='/espacio/:id' component={EspacioEdit} />
          <ProtectedRoute oute path='/espacio/new' component={EspacioEdit} />
          <ProtectedRoute path='/usuario' exact={true} component={UsuarioList} />
          <ProtectedRoute path='/usuario/:id' component={UsuarioEdit} />
          <ProtectedRoute path='/usuario/new' component={UsuarioEdit} />
          <ProtectedRoute path='/equipo' exact={true} component={EquipoList} />
          <ProtectedRoute path='/equipo/:id' component={EquipoEdit} />
          <ProtectedRoute path='/equipo/new' component={EquipoEdit} />
          <ProtectedRoute path='/reservar' exact={true} component={ReservarList} />
          <ProtectedRoute path='/reservar/:id' component={ReservarEdit} />
          <ProtectedRoute path='/reservar/new' component={ReservarEdit} />
        </Switch>
      </Router>
    )
  }
}

export default App;
