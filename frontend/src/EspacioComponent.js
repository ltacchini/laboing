import React from 'react';
import Select from 'react-select';

class EquipoComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [],
            selectedOption: this.props.selectedOption
        }
    }

    componentWillReceiveProps() {
        let { selected } = this.props;
        if (selected !== null && selected !== undefined) {
            selected = { value: selected.id, label: selected.nombre }
        }
        this.setState({ selectedOption: selected });
    }

    handleChange = (selectedOption) => {
        const { handleChange, items } = this.props;
        this.setState({ selectedOption });
        let options = [];

        if (selectedOption !== null) {
            for (let i = 0; i < items.length; i++) {
                if (selectedOption.value === items[i].id) {
                    options.push(items[i]);
                }
            }
        }
        handleChange(options);
    }
    prepareOptions(items) {
        let options = [];
        for (let i = 0; i < items.length; i++) {
            let text = items[i].nombre;
            options.push({ value: items[i].id, label: text });
        }
        return options;
    }

    render() {
        const { selectedOption } = this.state;
        const { isMulti, items } = this.props;
        const options = this.prepareOptions(items);

        return (
            <Select
                placeholder="Espacios..."
                value={selectedOption}
                onChange={this.handleChange}
                options={options}
                isMulti={isMulti}
                isOptionDisabled={(option) => option.activo === true}
                isClearable={true}
            />
        );
    }
}
export default EquipoComponent;