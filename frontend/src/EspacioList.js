import React, { Component } from 'react';
import { Button, ButtonGroup, Container} from 'reactstrap';
import AppNavbar from './AppNavbar';
import { Link, Redirect } from 'react-router-dom';
import matchSorter from 'match-sorter'
import ReactTable from "react-table";
import "react-table/react-table.css";

class EspacioList extends Component {

    constructor(props) {
        super(props);
        let rol;
        const cachedUser = localStorage.getItem('user');
        try {
            rol = JSON.parse(cachedUser).roles[0].role;
          } 
        catch (e) {
            this.state = { isOpen: false, isLoading: false, role: '' };
        }
        this.state = { espacio: [], isLoading: true, role: rol };
        this.remove = this.remove.bind(this);
    }

    componentDidMount() {
        this.setState({ isLoading: true });

        fetch('/$2y$12$D3x0eEB8CTk4Ubm96ySLeeLVzKBCbUSIs.gVBpGTHnaMiyejf8Eem')
            .then(response => response.json())
            .then(data => this.setState({ espacio: data, isLoading: false }));
    }

    async remove(id) {
        await fetch(`/espacios/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(() => {
            let updatedespacio = [...this.state.espacio].filter(i => i.id !== id);
            this.setState({ espacio: updatedespacio });
        });
    }

    render() {
        const { espacio, isLoading } = this.state;

        if (isLoading) {
            return <p>Cargando...</p>;
        }
        
        if (this.state.role === 'ADMIN') {
            return (
                <div>
                    <AppNavbar />
                    <Container fluid>
                        <div className="float-right">
                            <Button color="info" tag={Link} to="/espacio/new">Agregar espacio</Button>
                        </div>
                        <h3>Espacios</h3>
                        <ReactTable
                            previousText={'Pág. previa'}
                            nextText={'Pág. siguiente'}
                            pageText={'Página'}
                            rowsText={'filas'}
                            ofText={'de'}
                            data={espacio}
                            filterable
                            defaultFilterMethod={(filter, row) => String(row[filter.id]) === filter.value}
                            columns={[
                                {
                                columns: [
                                    {                          
                                        Header: "Codigo",
                                        id: "codigo",
                                        accessor: d => d.codigo,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["codigo"] }),
                                        filterAll: true
                                    },
                                    {                          
                                        Header: "Nombre",
                                        id: "nombre",
                                        style:{ 'whiteSpace': 'unset'},
                                        accessor: d => d.nombre,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["nombre"] }),
                                        filterAll: true
                                    },
                                    {                          
                                        Header: "Descripcion",
                                        id: "descripcion",
                                        style:{ 'whiteSpace': 'unset'},
                                        accessor: d => d.descripcion,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["descripcion"] }),
                                        filterAll: true
                                    },
                                    {                          
                                        Header: "Capacidad",
                                        id: "capacidad",
                                        accessor: d => d.cantidad,
                                        filterMethod: (filter, rows) =>
                                        matchSorter(rows, filter.value, { keys: ["capacidad"] }),
                                        filterAll: true
                                    },
                                    {
                                        Header: "Estado",
                                        accessor: "activo",
                                        id: "activo",
                                        Cell: ({ value }) => (value === true ? "Activo" : "Inactivo"),
                                        filterMethod: (filter, row) => {
                                            if (filter.value === "all") {
                                            return true;
                                            }
                                            if (filter.value === "true") {
                                                return row[filter.id] === true;
                                            }
                                                return row[filter.id] === false;
                                            },
                                        Filter: ({ filter, onChange }) =>
                                            <select
                                                onChange={event => onChange(event.target.value)}
                                                style={{ width: "100%" }}
                                                value={filter ? filter.value : "all"}
                                            >
                                            <option value="all">Mostrar todos</option>
                                            <option value="true">Activo</option>
                                            <option value="false">Inactivo</option>
                                            </select>
                                        },                                
                                        {
                                        Header: 'Acciones',
                                        id: "id",
                                        filterable: false,
                                        accessor: d => d.id,
                                        Cell: ({value}) => (
                                            <div>
                                                <td>
                                                <ButtonGroup>
                                                    <Button size="sm" color="info" tag={Link} to={"/espacio/" + value}>Editar</Button>
                                                    <Button size="sm" color="info" onClick={() => this.remove(value)}>Borrar</Button>
                                                </ButtonGroup>  
                                                </td>
                                            </div>
                                        )
                                        },
                                ]},
                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                    />            
                </Container>
                <div class="footer-copyright text-center py-3">© 2019:
                        Lic. en Sistemas - IDEI - UNGS
                </div>
            </div>
            );
        }
        else{
            return <Redirect to='/' />
        }   
    }
}

export default EspacioList;
