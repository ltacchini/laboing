package com.ltlg.erplab.model;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Reserva {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idReserva;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	@ManyToOne
	private Materia materia;
	@JsonFormat(pattern = "HH:mm:ss")
	private Time desde;
	@JsonFormat(pattern = "HH:mm:ss")
	private Time hasta;
	private Boolean activo;
	@ManyToOne
	private User user;
	@ManyToOne
	private Espacio espacio;
	@ManyToMany
	private List<Equipo> equipo;
	@ManyToOne
	private EstadoReserva estadoReserva;

	public Reserva() {
		super();
	}

	public Reserva(int idReserva, Boolean activo, Date dia, Time desde, Time hasta, User user, Espacio espacio,
			EstadoReserva estadoReserva, Materia materia, CantAlumno cantAlumno, List<Equipo> equipo) {
		super();
		this.idReserva = idReserva;
		this.activo = activo;
		this.fecha = dia;
		this.materia = materia;
		this.desde = desde;
		this.hasta = hasta;
		this.user = user;
		this.espacio = espacio;
		this.equipo = equipo;
		this.estadoReserva = estadoReserva;
	}

	public int getIdReserva() {
		return idReserva;
	}

	public List<Equipo> getEquipo() {
		return this.equipo;
	}

	public void setEquipo(List<Equipo> equipos) {
		this.equipo = equipos;
	}

	public void setIdReserva(int idReserva) {
		this.idReserva = idReserva;
	}

	public Date getDia() {
		return fecha;
	}

	public void setDia(Date dia) {
		this.fecha = dia;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Time getDesde() {
		return desde;
	}

	public void setDesde(Time desde) {
		this.desde = desde;
	}

	public Time getHasta() {
		return hasta;
	}

	public void setHasta(Time hasta) {
		this.hasta = hasta;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Espacio getEspacio() {
		return espacio;
	}

	public void setEspacio(Espacio espacio) {
		this.espacio = espacio;
	}

	public EstadoReserva getEstadoReserva() {
		return estadoReserva;
	}

	public void setEstadoReserva(EstadoReserva estadoReserva) {
		this.estadoReserva = estadoReserva;
	}

	@SuppressWarnings("deprecation")
	public boolean seSolapa(Reserva reserva) {
		if(reserva.fecha.getMonth() == this.fecha.getMonth() && reserva.fecha.getDate() == this.fecha.getDate() && reserva.fecha.getYear() == this.fecha.getYear()) {
			// La reserva inicia antes y finaliza despues que la reserva actual.
			if (reserva.desde.before(desde) && reserva.hasta.after(hasta))
				return true;

			// Hora de Inicio entre un horario reservado.
			else if (reserva.desde.after(desde) && reserva.desde.before(hasta))
				return true;

			// Hora de Fin entre un horario reservado.
			else if (reserva.desde.before(desde) && reserva.hasta.after(desde))
				return true;

			// Alguno de los horarios de inicio o fin se solapan con alguna reserva.
			else if (reserva.desde.equals(desde) || reserva.hasta.equals(hasta))
				return true;
		}
		return false;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	@Override
	public String toString() {
		return "Fecha " + this.fecha.toString() + " Desde " + this.desde.toString() + " Hasta " + this.hasta.toString();
	}
}