package com.ltlg.erplab.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Equipo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nro_inventario;
	private String nombre;
	private Boolean activo;
	private String descripcion;
	
	@ManyToOne
	private Marca marca;
	@ManyToOne
	private TipoProducto tipo;

	public Equipo() {
		super();
	}

	public Equipo(int id, String nro_inventario, Boolean activo, String nombre, String descripcion, Marca marca, TipoProducto tipo) {
		super();
		this.id = id;
		this.nro_inventario = nro_inventario;
		this.activo = activo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.marca = marca;
		this.tipo = tipo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public TipoProducto getTipo() {
		return tipo;
	}

	public void setTipo(TipoProducto tipo) {
		this.tipo = tipo;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getNro_inventario() {
		return nro_inventario;
	}

	public void setNro_inventario(String nro_inventario) {
		this.nro_inventario = nro_inventario;
	}

}