package com.ltlg.erplab.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ltlg.erplab.model.Equipo;

public interface EquipoRepository extends JpaRepository<Equipo, Integer>{

}
