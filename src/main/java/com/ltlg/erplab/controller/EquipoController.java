package com.ltlg.erplab.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ltlg.erplab.model.Equipo;
import com.ltlg.erplab.repository.EquipoRepository;

@RestController
public class EquipoController {

	@Autowired
	private EquipoRepository repository;

	@GetMapping("/equipos/{id}")
	public Optional<Equipo> get(@PathVariable("id") int id) {
		return repository.findById(id);
	}

	// Bcrypt "equipos", parche inmundo para que no se pueda acceder a los paths desde el deploy.
	@GetMapping("/$2y$12$7Ga4okJRF9E/PJPGN0UR..9WfPuhlhRz426VfqxwdWRy2VX8sojYm")
	public List<Equipo> all() {
		return repository.findAll();
	}

	@PostMapping("/equipos")
	public Equipo add(@RequestBody Equipo entity) {
		return repository.save(entity);
	}

	@DeleteMapping("/equipos/{id}")
	public List<Equipo> remove(@PathVariable("id") int id) {
		repository.deleteById(id);
		return repository.findAll();
	}

	@PutMapping("/equipos")
	public Equipo update(@RequestBody Equipo entity) {
		return repository.save(entity);
	}
}
