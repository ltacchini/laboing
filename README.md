# LaboIng

Sistema de Gestión para el laboratorio de ingenieria de la UNGS.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

  - #### NodeJs 
  - #### Java 8 
  - #### Maven 3 
  - #### MySQL 
  
### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

$ npm install npm -g


### Java 
- #### Java installation on Ubuntu

      $ sudo apt-get install openjdk-8-jre

### Maven
- #### Maven installation on Ubuntu

      $ sudo apt-get install maven

## Install

    $ git clone https://github.com/ltacchini/laboing
    $ cd laboing/frontend
    $ npm install

## Running the project

    $ cd laboing/frontend
    $ npm start
    
    In anoher terminal

    $ cd laboing/
    $ sudo mvn spring-boot:run
    

## Simple build for production

    $ /laboing/frontend/
    $ npm run build
    $ cd /laboing
    $ sudo mvn clean install
    $ java -jar target/erplab1-0.0.1-SNAPSHOT.jar 
    
## Authors

* **Lucas Grela** - [noname07](https://github.com/noname07)
* **Lautaro Tacchini** - [ltacchini](https://gitlab.com/ltacchini)


See also the list of [contributors](https://github.com/noname07/ERPLab2/contributors) who participated in this project.


